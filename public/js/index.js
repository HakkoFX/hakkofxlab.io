import { renderText, mapData } from "./startext_renderer.js";
function generateBanner()
{
    let elementsToRender = Array.from(document.getElementsByClassName("bannerContent"));

    elementsToRender.forEach((element) => {
        let text = element.children[0].innerHTML;
        let textToRender = mapData(text);
        renderText(element, textToRender);
    });
}

window.generateBanner = generateBanner;