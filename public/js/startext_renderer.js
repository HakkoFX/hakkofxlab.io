export function renderText(canvas, text)
{
    let context = canvas.getContext("2d");
    
    canvas.width = canvas.clientWidth;
    canvas.height = canvas.clientHeight;
    context.clearRect(0,0,canvas.width, canvas.height);
    let size = Math.min(canvas.width/text.length, canvas.height);
    for(let cnt = 0; cnt < text.length; cnt++)
    {
        /*let symbol = new Symbol(
            (canvas.width/2-size*text.length/2)+size*cnt+size/4,
            (canvas.height/2)-size/2,
            size,
            Character.pointMap[text[cnt]]);*/
        let symbol = new Symbol(
            size*cnt+size/4,
            (canvas.height/2)-size/2,
            size,
            Character.pointMap[text[cnt]]
        );
        symbol.render(context);
    }
}
export function mapData(text)
{
    let textToRender = [];
    let atAnno = "";
    text.split("").forEach((char) => {
        if(char == ' ')
        {
            if(atAnno.includes('@'))
            {
                atAnno += char;
            }
            else
            {
                textToRender.push("space");
            }
        }
        else if(char == '-')
        {
            if(atAnno.includes('@'))
            {
                atAnno += char;
            }
            else
            {
                textToRender.push("minus");
            }
        }
        else if(char == '@')
        {
            if(atAnno.includes('@'))
            {
                textToRender.push(atAnno.slice(1));
                atAnno = "";
            }
            else
            {
                atAnno += char;
            }
        }
        else 
        {
            if(atAnno.includes('@'))
            {
                atAnno+=char;
            }
            else
            {
                textToRender.push(char);
            }
        }
    });
    return textToRender;
}
class Symbol
{
    #x;
    #y;
    #size;
    #points = [];
    #edges = [];
    constructor(x, y, size, character) 
    {
        this.#x = x;
        this.#y = y;
        this.#size = size;
        character.points.forEach(point => {
            this.#points.push([point[0], point[1]]);
        });
        character.edges.forEach(edge => {
            this.#edges.push([edge[0], edge[1]]);
        });
    }
    render(context)
    {
        this.#points.forEach(point => {
            context.beginPath();
            context.arc(point[0]*this.#size+this.#x, point[1]*this.#size+this.#y, 2,0,Math.PI*2);
            context.shadowBlur= 10;
            context.shadowColor="rgba(255,255,255,1)";
            context.fillStyle="white";
            context.globalAlpha = 1;
            context.fill();
            context.shadowBlur = 0;
        });
        this.#edges.forEach(edge => {
            context.beginPath();
            context.moveTo(this.#points[edge[0]][0]*this.#size+this.#x, this.#points[edge[0]][1]*this.#size+this.#y);
            context.lineTo(this.#points[edge[1]][0]*this.#size+this.#x, this.#points[edge[1]][1]*this.#size+this.#y);
            context.strokeStyle = "rgba(255, 255, 255, 0.2)";
            context.stroke();
        });
    }
}
class Character
{
    static pointMap = {
        A : { 
            points: [[0.25,0],[0.125,0.5],[0.375,0.5],[0,1],[0.5,1]], 
            edges: [[3,1],[1,0],[0,2],[2,4],[1,2]] 
        },
        B: {
            points: [[0,0],[0,0.5],[0,1],[0.5,0.25],[0.5, 0.75]],
            edges: [[0,1],[1,2],[0,3],[3,1],[1,4],[4,2]]
        },
        C: {
            points: [[0.5, 0], [0,0.5],[0.5, 1]],
            edges: [[0,1],[1,2]]
        },
        D: {
            points: [[0,0], [0,1], [0.5, 0.5]],
            edges: [[0,1],[0,2],[1,2]]
        },
        E: {
            points: [[0,0],[0,0.5],[0,1],[0.5,0],[0.5,0.5],[0.5,1]],
            edges: [[0,1],[1,2],[0,3],[1,4],[2,5]]
        },
        F: {
            points: [[0,0],[0,0.5],[0,1],[0.5,0],[0.5, 0.5]],
            edges: [[0,1],[1,2],[0,3],[1,4]]
        },
        G: {
            points: [[0.25,0],[0,0.5],[0.25,1],[0.5,0.5],[0.25, 0.5]],
            edges: [[0,1],[1,2],[2,3],[3,4]]
        },
        H: {
            points: [[0,0], [0,0.5], [0,1],[0.5,0],[0.5, 0.5],[0.5, 1]],
            edges: [[0,1], [1,2], [3,4],[4,5],[1,4]]
        },
        I: {
            points:[[0.25,0],[0.25,1]],
            edges:[[0,1]]
        },
        J: {
            points:[[0,0],[0.5,0], [0.5,0.75],[0.25,1],[0,0.75]],
            edges:[[0,1],[1,2],[2,3],[3,4]]
        },
        K: {
            points: [[0,0], [0,0.5], [0,1],[0.5,0],[0.5, 1]],
            edges: [[0,1], [1,2], [3,1], [1,4]]
        },
        L: {
            points:[[0,0], [0,1], [0.5,1]],
            edges:[[0,1],[1,2]]
        },
        M: {
            points:[[0,0],[0,1],[0.25,0.5],[0.5, 0],[0.5,1]],
            edges:[[0,1],[0,2],[2,3],[3,4]]
        },
        N: {
            points:[[0,0],[0,1],[0.25,0.5],[0.5, 0],[0.5,1]],
            edges:[[0,1],[0,2],[2,4],[3,4]]
        },
        O: {
            points: [[0.25, 0],[0, 0.5],[0.5,0.5], [0.25,1]],
            edges: [[0,1],[1,3],[3,2],[2,0]]
        },
        P: {
            points:[[0,0],[0,0.5],[0,1],[0.5,0.25]],
            edges:[[0,1],[1,2],[0,3],[3,1]]
        },
        Q: {
            points:[[0.25, 0],[0, 0.5],[0.5,0.5], [0.25,1], [0.25,0.5],[0.5,1]],
            edges:[[0,1],[1,3],[3,2],[2,0],[4,5]]
        },
        R: {
            points:[[0,0],[0,0.5],[0,1],[0.5,0.25],[0.5,1]],
            edges:[[0,1],[1,2],[0,3],[3,1],[1,4]]
        },
        S: {
            points:[[0.5,0.25],[0.25,0],[0,0.25],[0.5,0.75],[0.25,1],[0,0.75]],
            edges:[[0,1],[1,2],[2,3],[3,4],[4,5]]
        },
        T: {
            points:[[0,0],[0.25,0],[0.5,0],[0.25,1]],
            edges: [[0,1],[1,2],[1,3]]
        },
        U: {
            points:[[0,0],[0,0.75],[0.25,1],[0.5,0.75],[0.5,0]],
            edges:[[0,1],[1,2],[2,3],[3,4]]
        },
        V: {
            points:[[0,0],[0.25,1],[0.5,0]],
            edges:[[0,1],[1,2]]
        },
        W: {
            points:[[0,0],[0,1],[0.25,0.5],[0.5,1],[0.5,0]],
            edges:[[0,1],[1,2],[2,3],[3,4]]
        },
        X: {
            points: [[0,0],[0,1],[0.5,0],[0.5,1],[0.25, 0.5]],
            edges: [[0,4],[1,4],[2,4],[3,4]]
        },
        Y: {
            points:[[0,0],[0.25,0.5],[0.5,0],[0.25,1]],
            edges: [[0,1],[1,2],[1,3]]
        },
        Z: {
            points:[[0,0],[0.5,0],[0,1],[0.5,1]],
            edges:[[0,1],[1,2],[2,3]]
        },
        a: {
            points:[[0,0.75],[0.5,0.5],[0.5,1]],
            edges:[[0,1],[1,2],[2,0]]
        },
        b: {
            points:[[0,0],[0,0.5],[0,1],[0.5,0.75]],
            edges:[[0,1],[1,2],[1,3],[2,3]]
        },
        c: {
            points:[[0,0.75],[0.5,0.5],[0.5,1]],
            edges:[[0,1],[0,2]]
        },
        d: {
            points:[[0.5,0],[0.5,0.5],[0.5,1],[0,0.75]],
            edges:[[0,1],[1,2],[1,3],[2,3]]
        },
        e: {
            points:[[0.25,0.5],[0,0.75],[0.5,0.75],[0.25,1]],
            edges:[[0,1],[1,2],[2,0],[1,3]]
        },
        f: {
            points:[[0.125,0],[0,0.25],[0,1]],
            edges:[[0,1],[1,2]]
        },
        g: {
            points:[[0.25,0.5],[0,0.75],[0.5,0.75],[0.25,1]],
            edges:[[0,1],[1,2],[2,0],[2,3]]
        },
        h: {
            points:[[0,0],[0,1],[0,0.5],[0.5,1]],
            edges:[[0,2],[2,1],[2,3]]
        },
        i: {
            points:[[0,0.5],[0,1]],
            edges:[[0,1]]
        },
        j: {
            points:[[0.125,0.5],[0.125,0.875],[0,1]],
            edges:[[0,1],[1,2]]
        },
        k: {
            points:[[0,0],[0,0.75],[0,1],[0.5,0.5],[0.5,1]],
            edges:[[0,1],[1,2],[1,3],[1,4]]
        },
        l: {
            points:[[0,0],[0,0.875],[0.125,1]],
            edges:[[0,1],[1,2]]
        },
        m: {
            points:[[0,1],[0,0.5],[0.25,0.5],[0.25,1],[0.5,1]],
            edges:[[0,1],[1,3],[3,2],[2,4]]
        },
        n: {
            points:[[0,1],[0,0.5],[0.25,1]],
            edges:[[0,1],[1,2]]
        },
        o: {
            points:[[0.25,0.5],[0,0.75],[0.25,1],[0.5,0.75]],
            edges:[[0,1],[1,2],[2,3],[3,0]]
        },
        p: {
            points:[[0,0.5],[0.5,0.5],[0,1]],
            edges:[[0,1],[1,2],[2,0]]
        },
        q: {
            points:[[0,0.5],[0.5,0.5],[0.5,1]],
            edges:[[0,1],[1,2],[2,0]]
        },
        r: {
            points:[[0,0.5],[0.5,0.5],[0,1]],
            edges:[[0,1],[2,0]]
        },
        s: {
            points:[[0,0.5],[0,1],[0.5,1],[0.5,0.5]],
            edges:[[0,3],[0,2],[1,2]]
        },
        t: {
            points:[[0.125,0],[0.125,1],[0,0.25],[0.25,0.25],[0.125,0.25]],
            edges:[[0,1],[2,3]]
        },
        u: {
            points:[[0,0.5],[0,1],[0.5,1],[0.5,0.5]],
            edges:[[0,1],[1,2],[2,3]]
        },
        v: {
            points:[[0,0.5],[0.25,1],[0.5,0.5]],
            edges:[[0,1],[1,2]]
        },
        w: {
            points:[[0,0.5],[0,1],[0.25,0.5],[0.25,1],[0.5,0.5]],
            edges:[[0,1],[1,2],[2,3],[3,4]]
        },
        x: {
            points:[[0,0.5],[0,1],[0.5,1],[0.5,0.5],[0.25,0.75]],
            edges:[[0,2],[1,3]]
        },
        y: {
            points:[[0,0.5],[0,1],[0.5,0.5],[0.25,0.75]],
            edges:[[0,3],[2,3],[3,1]]
        },
        z: {
            points:[[0,0.5],[0,1],[0.5,1],[0.5,0.5]],
            edges:[[0,3],[3,1],[1,2]]
        },
        star: {
            points:[[0,0]],
            edges:[]
        },
        hakkofx: {
            points:[[0,0.125],[0,0.625],[0.5,0.625],[0.5,0.125],[0.25,0.875]],
            edges:[[0,1],[1,2],[2,3],[0,2],[1,3],[1,4],[2,4]]
        },
        space: {
            points:[],
            edges:[]
        },
        minus: {
            points:[[0,0.5],[0.5,0.5]],
            edges:[[0,1]]
        }
    };
}